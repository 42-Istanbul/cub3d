/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_utils.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akarahan <akarahan@student.42istanbul.com. +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/29 22:32:11 by akarahan          #+#    #+#             */
/*   Updated: 2022/05/05 23:11:03 by akarahan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3D.h"

int	str_to_rgb(char *s)
{
	char	**rgb_arr;
	int		rgb;
	int		tmp;
	int		i;

	i = -1;
	rgb = 0;
	while (*s == 'C' || *s == 'F' || ft_isspace(*s))
		++s;
	check_rgb(s);
	rgb_arr = ft_split(s, ',');
	while (rgb_arr[++i])
	{
		if (!ft_isdigit_str(rgb_arr[i]))
			handle_errors(RGB_ERR);
		tmp = ft_atoi(rgb_arr[i]);
		if (tmp > 255 || tmp < 0)
			handle_errors(RGB_OUT_OF_BOUNDS);
		rgb |= tmp << 8 * (2 - i);
	}
	i = -1;
	while (rgb_arr[++i])
		free(rgb_arr[i]);
	free(rgb_arr);
	return (rgb);
}

char	*str_to_path(char *s)
{
	char	*path;

	while ((ft_isalpha(*s) || ft_isspace(*s)) && *s != '.')
		++s;
	path = ft_strtrim(s, " \n");
	return (path);
}

char	**ft_lst2tab(t_list *head, int *len, int *wd)
{
	char	**tab;
	int		max;
	int		i;

	i = -1;
	*len = ft_lstsize(head);
	tab = (char **)malloc(sizeof(char *) * (*len + 1));
	if (!tab)
		handle_errors(MALLOC_ERR);
	while (++i < *len)
	{
		tab[i] = (char *)head->content;
		max = ft_strlen(tab[i]);
		if (max > *wd)
			*wd = max;
		head = head->next;
	}
	tab[i] = NULL;
	return (tab);
}
