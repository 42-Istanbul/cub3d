/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle_errors.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akarahan <akarahan@student.42istanbul.com. +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/28 14:12:43 by akarahan          #+#    #+#             */
/*   Updated: 2022/05/03 18:16:15 by akarahan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3D.h"

void	handle_errors(int err)
{
	ft_putendl_fd("Error", STDERR);
	if (err == FILE_ERR)
		perror("File does not open: ");
	else if (err == MALLOC_ERR)
		perror("Malloc failed: ");
	else if (err == RGB_ERR)
		ft_putendl_fd("RGB is not digit", STDERR);
	else if (err == RGB_OUT_OF_BOUNDS)
		ft_putendl_fd("RGB is out of range", STDERR);
	else if (err == RGB_TOO_MANY)
		ft_putendl_fd("RGB too many numbers given", STDERR);
	else if (err == RGB_TOO_FEW)
		ft_putendl_fd("RGB too few numbers given", STDERR);
	else if (err == MAP_DUP_ERR)
		ft_putendl_fd("Map have duplicated player", STDERR);
	else if (err == MAP_EXCESS_CHAR)
		ft_putendl_fd("Map contains unrecognized char", STDERR);
	else if (err == MAP_NOT_CLOSED)
		ft_putendl_fd("Map is not closed", STDERR);
	else
		perror(NULL);
	exit(1);
}
