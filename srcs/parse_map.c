/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_map.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akarahan <akarahan@student.42istanbul.com. +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/21 13:08:40 by akarahan          #+#    #+#             */
/*   Updated: 2022/05/08 22:59:31 by akarahan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "cub3D.h"

void	ft_choose(t_list **head, t_map *map, char *line)
{
	if (ft_strnstr(line, "NO", ft_strlen(line)))
		map->no = str_to_path(line);
	else if (ft_strnstr(line, "SO", ft_strlen(line)))
		map->so = str_to_path(line);
	else if (ft_strnstr(line, "WE", ft_strlen(line)))
		map->we = str_to_path(line);
	else if (ft_strnstr(line, "EA", ft_strlen(line)))
		map->ea = str_to_path(line);
	else if (ft_strchr(line, 'F'))
		map->floor_clr = str_to_rgb(line);
	else if (ft_strchr(line, 'C'))
		map->ceil_clr = str_to_rgb(line);
	else if (*line != '\n' && *line != 0)
		ft_lstadd_back(head, ft_lstnew(ft_strtrim(line, "\n")));
}

int	find_zoom(t_map *data)
{
	float	zoom;

	zoom = 0;
	while (zoom * data->max_w < MINIMAP_W)
		zoom += 0.1;
	return ((int)zoom);
}

t_map	*parse_map(char *fname)
{
	t_map	*data;
	char	*line;
	int		fd;

	data = (t_map *)malloc(sizeof(t_map));
	init_map(data);
	fd = open(fname, O_RDONLY);
	if (fd < 0)
		handle_errors(FILE_ERR);
	line = get_next_line(fd);
	while (line)
	{
		ft_choose(&data->head, data, line);
		free(line);
		line = get_next_line(fd);
	}
	free(line);
	close(fd);
	data->map = ft_lst2tab(data->head, &data->map_h, &data->max_w);
	check_map(data);
	data->zoom = find_zoom(data);
	return (data);
}
